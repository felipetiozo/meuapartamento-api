const Schema = require('mongoose').Schema

const Model = module.exports = new Schema({
  price: Number,
  name: String,
  description: String,
  metadata: {
    useful_area: Number,
    rooms: Number,
    garages: Number,
    keys: Object
  },
  url: String,
  media: String,
  source: String
})

Model.static('findByInterest', async function(metadata = {}) {
  const metadataKeys = Object.keys(metadata).map(key => {
    return { [`metadata.keys.${key}`]: metadata[key] }
  })

  const possiblePosters = await this.find({
    $or: metadataKeys
  })

  const filteredPosters = possiblePosters.filter(poster => {
    const keys = Object.keys(possiblePosters.metadata.keys)

    const MIN_MATCH = Math.round(Object.keys(metadata).length * 0.75)

    let matches = 0

    keys.forEach(function(key) {
      if(metadata[key]) {
        matches++
      }
    })

    return matches >= MIN_MATCH
  })

  return filteredPosters
})