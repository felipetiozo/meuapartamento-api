const Schema = require('mongoose').Schema

const Model = module.exports = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  payload: Object
})