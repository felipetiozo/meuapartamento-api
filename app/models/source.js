const Schema = require('mongoose').Schema

const validSources = [
  'loft',
  'zapimoveis',
  'vivareal'
]

const Model = module.exports = new Schema({
  slug: {
    type: String,
    required: true,
    enum: validSources
  },

  name: {
    type: String,
    required: true
  }
})

Model.static('fetch', async function(slug) {
  const sourceFetches = require('@/helpers/source/index')

  if(!validSources.includes(slug)) {
    throw new Error(`This slug cannot be used: ${slug}`)
  }

  const sourceFetcher = sourceFetches[slug]

  if(!sourceFetcher) {
    throw new Error(`Invalid slug: ${slug}`)
  }

  const posters = await sourceFetcher()

  return posters
})

Model.static('syncPosters', async function(posters = []) {
  await Promise.all(posters.map(async poster => {
    return await Poster.update(
      {
        'poster.alias': poster.alias
      },
      {
        $set: poster
      },
      {
        upsert: true
      }
    )
  }))

  return posters.length
})