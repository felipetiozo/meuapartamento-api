const Schema = require('mongoose').Schema

const Model = module.exports = new Schema({
  campaign: {
    type: Schema.Types.ObjectId,
    ref: 'campaign'
  },

  email: {
    type: String
  }
})