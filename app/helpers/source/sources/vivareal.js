module.exports = function() {
  return [
    {
      price: 119000000,
      name: 'Avenida Nove de Julho 1273',
      description: 'Ap 48 - Jardim Paulista - 4º andar',
      metadata: {
        useful_area: 120,
        rooms: 3,
        garages: 1
      },
      url: 'https://www.vivareal.com.br/imovel/apartamento-2-quartos-jardim-paulista-zona-sul-sao-paulo-com-garagem-120m2-venda-RS1190000-id-2466133710/?__vt=bf:b',
      media: 'https://resizedimgs.vivareal.com/fit-in/870x653/vr.images.sp/bbd521fd457032f65efe1cb0c3026658.jpg',
      source: 'vivareal'
    },
    {
      price: 68000000,
      name: 'Alameda Joaquim Eugênio de Lima 1360',
      description: 'Ap 64 - Moema Passáros - 6º andar',
      metadata: {
        useful_area: 70,
        rooms: 1,
        garages: 1
      },
      url: 'https://www.vivareal.com.br/imovel/apartamento-1-quartos-jardim-paulista-zona-sul-sao-paulo-com-garagem-70m2-venda-RS680000-id-2461166887/?__vt=bf:b',
      media: 'https://resizedimgs.vivareal.com/fit-in/870x653/vr.images.sp/ba8c82bb22fef522dbe7aa2d7bfd25e2.jpg',
      source: 'vivareal'
    },
    {
      price: 34200900,
      name: 'Alameda Lorena 829',
      description: 'Ap 11 - Jardins - 1º andar',
      metadata: {
        useful_area: 22,
        rooms: 1,
        garages: 1
      },
      url: 'https://www.vivareal.com.br/imovel/apartamento-1-quartos-jardim-paulista-zona-sul-sao-paulo-22m2-venda-RS342009-id-2465914893/?__vt=bf:b',
      media: 'https://resizedimgs.vivareal.com/fit-in/870x653/vr.images.sp/0e95581e09daa17d861bc10bfb48bb70.jpg',
      source: 'vivareal'
    }
  ]
}