module.exports = function() {
  return [
    {
      price: 149500000,
      name: 'Alameda Franca 1581',
      description: 'Ap 21 - Jardim América - 2º andar',
      metadata: {
        useful_area: 112,
        rooms: 2,
        garages: 1
      },
      url: 'https://www.loft.com.br/home/zjzjj1',
      media: 'https://content.loft.com.br/homes/zjzjj1/banner_thumbnail.jpg',
      source: 'loft'
    },
    {
      price: 79000000,
      name: 'Rua Tuim 536',
      description: 'Ap 53 - Moema Passáros - 5º andar',
      metadata: {
        useful_area: 55,
        rooms: 2,
        garages: 1
      },
      url: 'https://www.loft.com.br/home/7nf1l0',
      media: 'https://content.loft.com.br/homes/7nf1l0/banner_thumbnail.jpg',
      source: 'loft'
    },
    {
      price: 62000000,
      name: 'Rua Doutor Veiga Filho 815',
      description: 'Ap 74 - Higienópolis - 7º andar',
      metadata: {
        useful_area: 62,
        rooms: 2,
        garages: 1
      },
      url: 'https://content.loft.com.br/homes/wvx805/banner_thumbnail.jpg',
      media: 'https://www.loft.com.br/home/wvx805',
      source: 'loft'
    }
  ]
}