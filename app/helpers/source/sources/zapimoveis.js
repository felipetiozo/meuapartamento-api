module.exports = function() {
  return [
    {
      price: 135000000,
      name: 'Alameda Joaquim Eugênio de Lima 1273',
      description: 'Ap 21 - Moema Passáros - 2º andar',
      metadata: {
        useful_area: 175,
        rooms: 3,
        garages: 1
      },
      url: 'https://www.zapimoveis.com.br/imovel/venda-apartamento-3-quartos-com-esgoto-jardim-paulista-zona-sul-sao-paulo-sp-175m2-id-2243265678/?__zt=bf%3Aa',
      media: 'https://resizedimgs.zapimoveis.com.br/fit-in/800x360/vr.images.sp/35a88ebf14baf6156101aa59ff46e11b.jpg',
      source: 'zapimoveis'
    },
    {
      price: 51000000,
      name: 'Alameda Santos 1360',
      description: 'Ap 34 - Cerqueira César - 3º andar',
      metadata: {
        useful_area: 51,
        rooms: 1,
        garages: 1
      },
      url: 'https://www.zapimoveis.com.br/imovel/venda-apartamento-1-quarto-com-armario-de-cozinha-jardim-paulista-zona-sul-sao-paulo-sp-51m2-id-2459933375/?__zt=bf%3Aa',
      media: 'https://resizedimgs.zapimoveis.com.br/fit-in/800x360/vr.images.sp/36ce2ca2c74d1a79df9b42d0ae4ba706.jpg',
      source: 'zapimoveis'
    },
    {
      price: 30000000,
      name: 'Rua José Maria Lisboa',
      description: 'Ap 13 - Jardim Paulista - 1º andar',
      metadata: {
        useful_area: 33,
        rooms: 1,
        garages: 1
      },
      url: 'https://www.zapimoveis.com.br/imovel/venda-apartamento-1-quarto-com-piscina-jardim-paulista-zona-sul-sao-paulo-sp-33m2-id-2442317212/?__zt=bf%3Aa',
      media: 'https://resizedimgs.zapimoveis.com.br/fit-in/800x360/vr.images.sp/9d8e691d9468e190e7d08b0f28da954f.jpg',
      source: 'zapimoveis'
    }
  ]
}