const _ = require('lodash')

module.exports = function getInnerFieldValue(obj, path) {
  let val = _.get(obj, path)
  if (!val) return null
  // map to an array of values
  if (_.isArray(val)) {
    return val.map(v => v._id ? v._id : v)
  }
  // tries to get field id
  return val._id ? val._id : val
}