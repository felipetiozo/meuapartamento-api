const _ = require('lodash')

module.exports = function validatePhone(value) {
  value = value.replace("(", "")
  value = value.replace(")", "")
  value = value.replace("-", "")
  value = value.replace(" ", "").trim()

  response = { valid: true }

  if(_.isNumber(value)) value = String(value)

  let DDDs = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "20", "23", "25", "26", "29", "30", "36", "39", "40", "50", "52", "56", "57", "58", "59", "60", "70", "72", "76", "78", "80", "90"]
  let DDD = value.substring(0, 2)

  if (DDDs.indexOf(DDD) != -1 && DDD.length == 2) {
    response = { valid: false, data: 'Número do DDD inválido' }
  }

  if (value.length > 0 && ![10, 11].includes(value.length)) {
    response = { valid: false, data: 'Tamanho do número inválido' }
  }

  if (value == '0000000000') {
    response = { valid: false, data: 'Número inválido' }
  }

  return response.valid
}