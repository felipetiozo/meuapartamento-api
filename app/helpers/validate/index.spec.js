const validate = require('./index')
const assert = require('assert')

describe('app.helpers.validate', function () {
  let Fields

  beforeEach(function () {
    Fields  = [
      {
        name: 'cpf',
        as: 'alias',
        validate: (val) => true,
        transform: (val) => val * 2,
        required: true,
      },
      {
        name: 'cnpj',
        as: 'superalias'
      }
    ]
  })

  it('omit extra params, call transform and alias', async function () {
    let payload = {
      payload: { cpf: 123, test: 1, opa: 2 },
      fields: Fields 
    }

    let resp = await validate(payload)

    assert.equal(resp.alias, 246)
  })

  describe('required', async function () {
    it('throws if required is unset', async function () {
      let payload = {
        payload: { test: 1, opa: 2 },
        fields: Fields,
      }

      await assertFuncThrows('MissingParameter', validate, payload)
    })

    it('allow required as a function', async function () {
      let test = function () {
        return this.payload.test == 1
      }

      let payload = {
        payload: { test: 1, opa: 2 },
        fields:  [{
          name: 'cpf',
          as: 'alias',
          required: test
        }, 
        {
          name: 'test'
        }]
      }

      await assertFuncThrows('MissingParameter', validate, payload)
    })
  })

  it('throws if validate fails', async function () {
    let payload = {
      payload: { cpf: 1, test: 1, opa: 2 },
      fields:  [{
        name: 'cpf',
        as: 'alias',
        validate: (val) => false,
        transform: (val) => val * 2,
      }]
    }

    await assertFuncThrows('InvalidParameter', validate, payload)
  })

  it('does not update the field if is not updatable', async function () {
    let payload = {
      payload: { cpf: 1, test: 1, opa: 2 },
      fields: [{
        name: 'cpf',
        as: 'alias',
        transform: (val) => val * 2,
        editable: false
      }],
      editing: true,
    }

    let resp = await validate(payload)
    assert.deepEqual(resp, {})
  })

  it('push more validators based on custom validators', async function () {
    let payload = {
      payload: { roles: ['team:mentor']},
      fields: [{
        name: 'roles',
        as: 'roles',
        validate: function (val) {
          if(val.includes('team:mentor')) {
            this.fields.push({
              name: 'cpf',
              as: 'cpf',
              required: true
            })
          }
          return true
        }
      }]
    }

    await assertFuncThrows('MissingParameter', validate, payload)
  })

  it('it accepts a function as editing rule', async function () {
    let payload = {
      payload: { roles: ['team:mentor'], status: 'locked'},
      editing: true,
      fields: [{
        name: 'roles',
        as: 'roles',
        editable: function (val) {
          return ['allowed', 'analysis'].includes(this.payload.status)
        }
      }],
    }

    let resp = await validate(payload)
    assert.deepEqual(resp, {})
  })

  it('allowed fields to be passed as a function', async function () {
    let payload = {
      payload: { cpf: '123'},
      editing: true,
      fields: function (){
        return [
          {
            name: 'cpf',
            as: `fields.${this.community}:some-category.cpf`
          }
        ]
      },
      community: 'test'
    }

    let resp = await validate(payload)
    assert.deepEqual(resp, {
      fields: {
        'test:some-category': { cpf: '123' } 
      }
    })
  })

  describe('only parameter', function () {
      it('continues to process a field if only is true', async function () {
        let payload = {
          payload: { roles: ['team:mentor'], mentor: 'abc'},
          fields: [{
            name: 'roles',
            as: 'roles',
          }, {
            name: 'mentor',
            only: function () {
              return (this.payload.roles || []).includes('team:mentor')
            }
          }]
        }

        let resp = await validate(payload)
        assert.deepEqual(resp, { roles: ['team:mentor'], mentor: 'abc'})
      })

      it('does not process field', async function () {
        let payload = {
          payload: { roles: ['team:mentor'], mentor: 'abc'},
          fields: [{
            name: 'roles',
            as: 'roles',
          }, {
            name: 'mentor',
            only: function () {
              return false
            }
          }]
        }

        let resp = await validate(payload)
        assert.deepEqual(resp, { roles: ['team:mentor'] })
      })
  })
})