const _ = require('lodash')
const errors = require('@/errors')

module.exports = async function (params) {
  let response = {}

  params = _.defaults(params, {
    payload: {},
    fields: [],
    editing: false,
    errorScope: '',
  })

  // if fields is a function call it
  params.fields = _.isFunction(params.fields) ? params.fields.call(params) : params.fields

  for(var i = 0; i < params.fields.length; i++) {
    let field = params.fields[i]

    // get params.fields value
    let value = _.get(params.payload, field.name, undefined)

    // check if this field should appear
    let isOnly = field.only && _.isFunction(field.only) ? field.only.call(params, value) : true

    if(!isOnly) continue

    // check if editable is a function, if so call it
    let isRequired = _.isFunction(field.required) ? await field.required.call(params, value) : _.get(field, 'required', false)

    // if field is not set but is required, throws
    if(value == undefined && isRequired && !params.editing) {
      let message = params.errorScope ? `${params.errorScope}.${field.name}` : field.name
      throw new errors.BadRequest.MissingParameter(message)
    }

    // check if editable is a function, if so call it
    let isEditable = _.isFunction(field.editable) ? await field.editable.call(params, value) : _.get(field, 'editable', true)

    // if editing but field is not editable, throws
    if(value != undefined && params.editing && !isEditable) {
      // check if we have a previous snapshot of this and if value changed
      let previousValue = _.get(params.previous, field.name, undefined)
      if(previousValue == undefined) {
        continue
      }
      if(!_.isEqual(previousValue, value)){
        let message = params.errorScope ? `${params.errorScope}.${field.name}` : field.name
        throw new errors.Forbidden.CannotModify(message)
      }
    }

    if(value == undefined) continue

    // validate this field
    let isValid = field.validate ? await field.validate.call(params, value) : true

    if(_.isObject(isValid) ? !isValid.valid : !isValid) {
      let customName = params.errorScope ? `${params.errorScope}.${field.name}` : field.name
      let message = _.castArray(_.get(isValid, 'message', customName))
      throw new errors.BadRequest.InvalidParameter(...message)
    }

    // transform field if necessary
    let newValue = _.isFunction(field.transform) ? field.transform.call(params, value) : value

    // set value on field
    _.set(response, field.as || field.name, newValue)
  }

  return response
}