const app = require('@/app')
const errors = require('@/errors')

module.exports = async function(context) {
  const Interest = app.models.interest

  const payload = context.body.payload

  if(!payload) {
    throw new errors.BadRequest(`Missing payload: ${payload}`)
  }

  await Interest.create({ payload })
}