const app = require('@/app')
const errors = require('@/errors')

module.exports = async(context) => {
  return {
    status: 'alive',
    version: app.config.VERSION,
    date: Date.now()
  }
}