const app = require('@/app')

module.exports = async function(context) {
  const Poster = app.models.poster

  const limit = 9

  const anwsers = context.query.anwsers || null

  const query = {}

  if(anwsers) {
    query[anwsers] = anwsers
  }

  const page = context.query.page || 0

  const posters = await Poster
    .find(query)
    .skip(Number(page * limit))
    .limit(limit)

  return {
    hits: posters,
    count: await Poster.count(query)
  }
}