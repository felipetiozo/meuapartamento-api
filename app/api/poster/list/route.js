const app = require('@/app')

module.exports = async(router) => {
  router.get('/poster',
    app.helpers.routes.func(require('./func.js')))
}