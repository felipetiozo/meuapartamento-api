const app = require('@/app')

module.exports = async function(context) {
  const Poster = app.models.poster

  await Poster.remove({})

  return { success: true }
}