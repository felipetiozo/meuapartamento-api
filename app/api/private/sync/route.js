const app = require('@/app')

module.exports = async(router) => {
  router.get('/private/sync',
    app.helpers.routes.func(require('./func.js')))
}