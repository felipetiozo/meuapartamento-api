const app = require('@/app')
const sourceFetches = require('@/helpers/source/index')

module.exports = async function(context) {
  const sources = ['zapimoveis', 'loft', 'vivareal']

  sources.forEach(async slug => {
    const source = sourceFetches[slug]

    const posters = source()

    await app.models.poster.create(posters)
  })

  return { success: true }
}