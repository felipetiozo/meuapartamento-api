const app = require('@/app')

const DEFAULT_OPTIONS = {
  timezone: 'America/Sao_Paulo',
}

module.exports = function (agenda) {
  agenda.on('ready', async function() {
    // agenda.every('5 minutes', 'exportValidates', {}, DEFAULT_OPTIONS)


    agenda.start()
  })
}