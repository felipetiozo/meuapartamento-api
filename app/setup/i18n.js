const i18n = require('i18n')
const path = require('path')

// Load helpers into app.helper
module.exports = async(app) => {
  i18n.configure({
    locales: ['en', 'pt-BR'],
    directory: path.join(__dirname, '../locales'),
    objectNotation: true,
    updateFiles: false,
  })

  return i18n
}