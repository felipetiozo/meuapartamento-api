/*
 * Load models and register with Mongoose
 */
const _ = require('lodash')
const mongoose = require('mongoose')
const requireSmart = require('require-smart')

const PluginTimestamp = require('mongoose-timestamp')
const PluginSlug = require('mongoose-slug-generator');


module.exports = async (app) => {
  let schemas = requireSmart('../models', {
    skip: [/\..*\.js$/],
  })

  // Return loaded models
  return await walkModels(app, schemas)
}

// recursively walk into models folders
async function walkModels(app, schemas) {
  let models = {}

  for (let name in schemas) {
    let Schema = schemas[name]

    // check if we are dealing with a schema or a subfolder
    if (Schema instanceof mongoose.Schema) {
      Schema.plugin(PluginTimestamp)
      Schema.plugin(PluginSlug)

      // Load model into mongo connection
      models[name] = app.mongo.model(name, Schema, name)
    } else {
      // Just append to the tree, but don't load it as a Mongoose model
      models[name] = Schema
    }
  }

  return models
}