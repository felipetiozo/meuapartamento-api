const express = require('express')
const fallback = require('express-history-api-fallback')

/*
 * Serve assets on app.config.DIST_FOLDER
 */
module.exports = async (app) => {
  let static = express.static(app.config.distFolder, {
    maxAge: app.config.maxAge,
  })

  app.server.use(static)
}
