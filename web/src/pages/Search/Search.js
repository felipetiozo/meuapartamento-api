import _ from 'lodash'
import Interest from '@/services/Interest'

function writeWithDelay(id, text, delay = 40, pointDelay = 1500) {
  let element = document.getElementById(id)
  let letters = [...text.split('')]
  element.textContent = element.textContent + letters[0]
  letters = letters.splice(1)
  text = letters.join("")
  if(letters[0] == '.') delay = pointDelay
  setTimeout(() => {
    if(text) writeWithDelay(id, text)
  }, delay)
}

export default {
  name: "Search",

    data() {
    return {
      address: null,
      loading: false,
      step: 0,
      places: [{
         "description" : "São Paulo, SP, Brasil",
         "id" : "fedb05012f42e79f038a58eac44e1bbc61b7c7aa",
         "matched_substrings" : [
            {
               "length" : 9,
               "offset" : 0
            }
         ],
         "place_id" : "ChIJ0WGkg4FEzpQRrlsz_whLqZs",
         "reference" : "ChIJ0WGkg4FEzpQRrlsz_whLqZs",
         "structured_formatting" : {
            "main_text" : "São Paulo",
            "main_text_matched_substrings" : [
               {
                  "length" : 9,
                  "offset" : 0
               }
            ],
            "secondary_text" : "SP, Brasil"
         },
         "terms" : [
            {
               "offset" : 0,
               "value" : "São Paulo"
            },
            {
               "offset" : 11,
               "value" : "SP"
            },
            {
               "offset" : 15,
               "value" : "Brasil"
            }
         ],
         "types" : [ "locality", "political", "geocode" ]
      }],
      search: null,
      loadingSearch: false,

      bairros: [
        'Jardim América',
        'Jardim Paulista',
        'Itaim Bibi',
        'Vila Nova Conceição',
        'Moema Passáros',
        'Moema Índios',
        'Jardim Europa',
        'Higienópolis',
        'Pinheiros',
        'Vila Primareva',
        'Vila Olímpia',
        'Perdizes',
        'Vila Madalena',
        'Chácara Klabin'
      ],

      priceOptions: [
        {
          key: '1',
          value: 50000000,
          label: 'R$500.000'
        },
        {
          key: '2',
          value: 100000000,
          label: 'R$1.000.000'
        },
        {
          key: '3',
          value: 150000000,
          label: 'R$1.500.000'
        },
      ],

      lifeOptions: [
        {
          key: '1',
          value: 'alone',
          label: 'Morando sozinho'
        },
        {
          key: '2',
          value: 'invest',
          label: 'Investindo o meu dinheiro'
        },
        {
          key: '3',
          value: 'upsizing',
          label: 'Minha familia cresceu'
        },
        {
          key: '4',
          value: 'downsizing',
          label: 'Minha familia diminuiu'
        }
      ],

      showName: true,
      name: null,
      age: null,
      neighborhood: null,
      priceOption: null,
      lifeOption: null,
      options: {
        lazer: null,
        gym: null,
        pool: null,
        games: null
      },
      images: {}
    }
  },


  mounted() {
    writeWithDelay('welcome', 'Olá, seja bem-vindo ao Meu Apê 😀🎉', 20)
    setTimeout(() => {
      writeWithDelay('welcome2', 'Nós iremos te ajudar à encontrar o apê dos sonhos 🤩. Para começar, como posso te chamar?', 40, 2500)
    }, 3200)
  },

  watch: {
    search(val) {
      this.loadingSearch = true
      this.searchDebounced(val)
    },

    name(val) {
      if(val.length >= 2) {
        this.showName = true
      }
    },

    async step(newVal) {
      let element = document.getElementById('welcome2')
      let messages = [
        `Ok ${this.name}, desculpa perguntar, mas qual é a sua idade? Não iremos contar pra ninguém, eu prometo.`,
        `Saudades dessa idade! Mas me diz, qual bairro você gostaria de morar em São Paulo/SP?`,
        `Legal, e até quanto você estaria disposto à pagar por um apê?`,
        `E em qual desses momentos você se encontra?`,
        `Desses itens, quais você gostaria de ter no condôminio?`,
        `Me diz ${this.name}, qual dessas decorações você mais gostou?`,
        `E qual dessas vistas você mais gostaria?`,
        `Por último, me fala uma sacada que faz seu estilo =)`,
        `Agora aguarde um pouquinho porquê estamos achando o seu próximo apartamento...`
      ]


      element.textContent = ''

      writeWithDelay('welcome2', messages[newVal - 1])

      if(newVal == 9) {
        const payload = {
          name: this.name,
          age: this.age,
          neighborhood: this.neighborhood,
          priceOption: this.priceOption,
          lifeOption: this.lifeOption,
          options: this.options,
          images: this.images
        }
        try {
          await Interest.create({ payload })
        } catch(err) {
          console.log(err)
        }
        setTimeout(() => {
          this.$router.push('/list')
        }, 7000)
      }
    }
  },

  computed: {
    entries() {
      return ([]).concat(this.places)
    },

    stepValidations() {
      return {
        "0": this.name,
        "1": this.age,
        "2": this.neighborhood,
        "3": this.priceOption,
        "4": this.lifeOption
      }
    }
  },

  methods: {
    next(){
      if(this.stepValidations[this.step] && !this.stepValidations[this.step]) return

      this.step++
    },

    setImage(kind, image) {
      this.images[kind] = image
      this.next()
    },

    searchDebounced: _.debounce(async function (newVal) {
      // this.searchAddresses(newVal)
    }, 500, {leading: false, trailing: true}),
  },
}