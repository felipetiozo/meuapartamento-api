import Poster from '@/services/Poster'

export default {
  name: "List",

  data() {
    return {
      posters: [],
      count: 0
    }
  },

  async created() {
    try {
      let data = (await Poster.list()).data
      this.posters = data.hits
      this.count = data.count
    } catch(err) {
      console.log(err)
    }
  },

  methods: {
    source(method) {
      return {
        'zapimoveis': 'ZAP Imóveis',
        'loft': 'Loft',
        'vivareal': 'Viva Real'
      }[method]
    },

    openWPP(poster) {
      let whatsapp_number
      if(poster.source == 'zapimoveis') whatsapp_number = '551140059111'
      if(poster.source == 'vivareal') whatsapp_number = '551140059111'
      if(poster.source == 'loft') whatsapp_number = '551141185638'
      let message = 'Olá, quero comprar o ' + poster.name
       window.open('https://web.whatsapp.com/send?phone='+whatsapp_number+'&text='+message)
    }
  }
}