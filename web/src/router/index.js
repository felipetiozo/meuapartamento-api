import Vue from 'vue'
import Router from 'vue-router'

// Services
import NotFound from '@/pages/NotFound'

// Pages
import Search from '@/pages/Search'
import List from '@/pages/List'

import _ from 'lodash'
Vue.use(Router)

var router = new Router({
  mode: 'history',
  routes: [
    {
      alias: '/',
      path: '/search',
      name: 'Search',
      component: Search,
      meta: {
        title: 'Search',
      },
    },
    {
      path: '/list',
      name: 'List',
      component: List,
      meta: {
        title: 'List',
      },
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound,
      meta: {
        title: 'Página não encontrada',
      },
    },
  ]
})


router.beforeEach(function (to, from, next) {

  // Check backend status
  Axios.get('/status').then((res) => {
  }, (err) => {
    return next({
      name: 'NotFound',
      // query: { redirect: to.fullPath }
    })
  })

   /*
   * Change title
   */
  let title = 'Meu Apê'
  let parts = to.matched
    .filter(route => route.meta && route.meta.title)
    .map(route => route.meta.title)

  if (parts.length > 0) {
    title = parts.join(' > ')
  }
  document.title = title

  next()
})


export default router
