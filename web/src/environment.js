// Find out where it's running (localhost, test server or production)
function APILocation() {
  const URL = window.location.href
  if (URL.indexOf('meuapartamento.herokuapp.com') > 0) {
    return 'https://meuapartamento.herokuapp.com/v1'
  }

  if (URL.indexOf('localhost') > 0) {
    return 'https://meuapartamento.herokuapp.com/v1'
  }

  // Defaults to production api
  return 'https://meuapartamento.herokuapp.com/v1'
}

export default {
  API_URL: APILocation(),
}
