export default function Deferred() {
  let resolve, reject
  let promise = new Promise(function(_resolve, _reject) {
    reject = _reject
    resolve = _resolve
  })

  promise._reject = reject
  promise._resolve = resolve

  return promise
}