import Vue from 'vue'
import moment from 'moment'

// Init nonce to make relative time reactive
let nonce = {}
Vue.util.defineReactive(nonce, 'fast', 0)
Vue.util.defineReactive(nonce, 'slow', 0)
setInterval(() => nonce.fast++, 10000)
setInterval(() => nonce.slow++, 60000)


export default function (value, format, utc = false) {
  if (!format) {
    format = 'DD/MM/YY'
  }

  else if (format == 'datetime') {
    format = 'DD/MM/YY [às] HH:mm'
  }

  else if (format == 'slim') {
    format = 'DD MMM, HH:mm'
  }

  else if (format == 'age') {
    return value && moment().diff(moment(String(value)), 'year')
  }

  else if (format == 'fromNow') {
    let date = value && moment(value)
    let diff = date && moment().diff(date)
    
    if (Math.abs(diff) < 120 * 1000) {
      nonce.fast
    } else {
      nonce.slow
    }

    return date && date.fromNow()
  }

  else if (format == 'duration') {
    return value && moment.duration(value).humanize()
  }

  else if(format == 'longtime') {
    format = 'DD MMM YY [às] HH:mm'
  }

  else if(format == 'longerDate') {
    format = 'DD MMM YYYY'
    return moment(String(value)).format(format).toLowerCase()
  }

  if(value && utc) {
    return moment.utc(String(value)).format(format)
  }

  if (value) {
    return moment(String(value)).format(format)
  }
}