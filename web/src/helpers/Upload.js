import Vue from 'vue'
import Axios from 'axios'
import { CancelToken } from 'axios'

import Deferred from '@/helpers/Deferred'
import Community from '@/services/Community'
import Environment from '@/environment'
import Auth from '@/services/Auth'

export default class Upload {
  constructor(file, options = {}) {
    this.options = options

    // Save file locally
    this.file = file
    this.name = file.customName || file.name

    // Define reactive properties
    Vue.util.defineReactive(this, 'info', null)
    Vue.util.defineReactive(this, 'error', null)
    Vue.util.defineReactive(this, 'status', null)
    Vue.util.defineReactive(this, 'progress', null)

    // Create the promise that will be returned
    this.request = null
    this.promise = Deferred()

    // Catch error and save msg
    this.promise.catch(e => this.error = String(e))

    // Cancelation token
    this.cancelToken = null

    // Current status
    this.setStatus('queued')
    
    if (options.autostart) {
      this.start()
    }
  }

  get running() {
    return this.status == 'executing' || this.status == 'queued'
  }

  get finished() {
    return this.status == 'completed' || this.status == 'canceled' || this.status == 'failed'
  }

  start() {
    // Return Defered promise (doesnt starts)
    if (this.status == 'executing' || this.status == 'completed') {
      return this.promise
    }

    // Cannot re-start a failed upload or canceled one
    if (this.status == 'failed' || this.status == 'canceled') {
      // throw new Error('cannot re-start failed upload')
      // return new Upload(this.file, true)
      return this.promise
    }

    // Queued, then start it
    if (this.status == 'queued') {
      this._makeRequest()
      return this.promise
    }

    throw new Error('Invalid state: ' + this.status)
  }

  _makeRequest() {
    if (this.status != 'queued') {
      throw new Error('can only _makeRequest on `queued` upload')
    }


    // Prepare payload
    let data = new FormData()
    data.append('file', this.file)

    // Create cancelation token
    this.cancelToken = CancelToken.source()

    // Prepare progress hooks and etc.
    let config = {
      method: 'post',
      url: Environment.API_URL + '/media/upload?_community=' + Community.community,
      params: {
        name: this.name
      },
      data: data,
      cancelToken: this.cancelToken.token,
      headers: {
        'Authorization': 'Bearer ' + Auth.currentToken
      },
      onUploadProgress: (progressEvent) => {
        this.progress = progressEvent.loaded / progressEvent.total
        this.options.progress && this.options.progress(progressEvent)
      },
    }

    // Set to executing
    this.setStatus('executing')

    this.request = Axios(config)
    .then(res => {
      this.setStatus('completed')

      // Set media info
      this.info = res.data

      // Resolve defered promise
      this.promise._resolve(res.data)
    }).catch(err => {
      let canceled = Axios.isCancel(err)

      // Set status based on canceled flag
      this.setStatus(canceled ? 'canceled' : 'failed')

      // Reject defered promise
      this.promise._reject(err)
    })
  }

  cancel(msg = 'canceled') {
    if (this.finished) {
      // Does nothing, because it already finished
    } else if (this.status == 'queued') {
      // Since the upload didnt start yet, set status to canceled and reject promise
      this.setStatus('canceled')
      this.promise._reject(msg)
    } else if (this.status == 'executing') {
      // Cancel the request with the cancelation token
      this.cancelToken && this.cancelToken.cancel(msg)
    } else {
      throw new Error('Invalid state: ' + this.status)
    }
  }

  setStatus(status) {
    this.status = status
  }
}

