export default {
  beforeDestroy: function () {
    this.$intervals && this.$intervals.map(t => clearInterval(t))
    this.$intervals = undefined

    this.$timeouts && this.$timeouts.map(t => clearTimeout(t))
    this.$timeouts = undefined
  },

  methods: {
    $setInterval: function (fn, time) {
      let interval = setInterval(fn, time)
      this.$intervals = this.$intervals || []
      this.$intervals.push(interval)
      return interval
    },

    $setTimeout: function (fn, time) {
      let timeout = setTimeout(fn, time)
      this.$timeouts = this.$timeouts || []
      this.$timeouts.push(timeout)
      return timeout
    },
  }
}