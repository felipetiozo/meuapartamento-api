
import moment from 'moment'

export default function (startDate, endDate, format = 'short') {
  if(!startDate || !endDate) return

  startDate = moment.utc(startDate)
  endDate = moment.utc(endDate)

  let daysDiff = moment(endDate - startDate).format('D')
  if(daysDiff < 0) return

  let dateFormat = format == 'long' ? 'DD [de] MMMM' : 'DD/MMM'
  
  if(daysDiff < 1) return startDate.format(dateFormat + ' [de] YYYY')
  if(daysDiff >= 1 && daysDiff < 3) return startDate.format(dateFormat) + ' à ' + endDate.format(dateFormat + ' [de] YYYY')

  // Diference better than 3 days
  if(daysDiff >= 3) return startDate.format(dateFormat) + ' até ' + endDate.format(dateFormat + ' [de] YYYY')

}