import Axios from 'axios'
window.Axios = Axios

import Environment from '@/environment'

// Add HTTP response interceptor
Axios.interceptors.response.use(null, function (error) {
  // Checks if Forbidden
  const status = error.response && error.response.status
  if (status === 401) {
    // Auth.signOut()
  }

  return Promise.reject(error);
})

Axios.interceptors.request.use(function (config) {
  // Enable to make requests in other URLs (without being our API)
  console.log('interceptor request', config.baseURL, config.url)
  let isBase = config.baseURL == Environment.API_URL
  if (isBase && config.url.startsWith('http')) {
    return config;
  }

  // Add Authorization header
  // if (Auth.currentToken) {
  //   config.headers['Authorization'] = 'Bearer ' + Auth.currentToken
  // }

  // Add community-id
  if(!('params' in config)) {
    config.params = {}
  }

  return config;
})

// Setup base URL for Axios
Axios.defaults.baseURL = Environment.API_URL