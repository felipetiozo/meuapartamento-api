/*
 * Filters
 */
import Vue from 'vue'

import FormatEventDate from '@/helpers/FormatEventDate'
Vue.filter('FormatEventDate', FormatEventDate)

import FormatDate from '@/helpers/FormatDate'
Vue.filter('FormatDate', FormatDate)

import MimeIcon from '@/helpers/MimeIcon'
Vue.filter('MimeIcon', MimeIcon)