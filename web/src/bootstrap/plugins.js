/*
 * Load Plugins into Vue
 */ 
import Vue from 'vue'

import Vuetify from 'vuetify'
Vue.use(Vuetify)

import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

// Necessário para FireFox/IE
import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

import ElementUI from 'element-ui'
Vue.use(ElementUI)

import VeeValidate, { Validator } from 'vee-validate'
Vue.use(VeeValidate)

import VeeLocale_pt_BR from 'vee-validate/dist/locale/pt_BR'
Validator.localize('pt_BR', VeeLocale_pt_BR)

import Moment from 'moment'
Moment.locale('pt-BR')