/*
 * Load CSS's
 */
import 'element-ui/lib/theme-chalk/index.css'
import 'vuetify/dist/vuetify.min.css'
import 'font-awesome/css/font-awesome.css'
import '@mdi/font/css/materialdesignicons.css'


/*
 * CSS Styles Global and Overrides
 */

import '@/styles/App.css'
import '@/styles/Borders.css'
import '@/styles/Colors.css'
import '@/styles/Grid.css'
import '@/styles/Elevate.css'
import '@/styles/Text.css'
import '@/styles/VuetifyOverrides.css'
import '@/styles/ElementOverrides.css'
import '@/styles/Animations.css'