import Raven from 'raven-js'

// only add raven if we are in production
const URL = window.location.href
if (URL.indexOf('kontromatic.herokuapp.com') > 0) {
  Raven
    .config('https://5f3d92ec2b0f45d49499e204f906d1f8@sentry.io/1814750')
    .install()
}
