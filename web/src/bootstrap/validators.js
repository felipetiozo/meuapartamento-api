/*
 * Custom Validators
 */
import {Validator} from 'vee-validate'
Validator.extend('phone', {
  getMessage(fields, params, data) {
    return data || 'Algo deu errado'
  },
  validate(value) {

    return new Promise(resolve => {
      let response = {}

      value = value.replace("(", "")
      value = value.replace(")", "")
      value = value.replace("-", "")
      value = value.replace(" ", "").trim()

      response = { valid: true }

      if(_.isNumber(value)) value = String(value)

      let DDDs = ["00", "01", "02", "03", , "04", , "05", , "06", , "07", "08", "09", "10", "20", "23", "25", "26", "29", "30", "36", "39", "40", "50", "52", "56", "57", "58", "59", "60", "70", "72", "76", "78", "80", "90"]
      let DDD = value.substring(0, 2)

      if (DDDs.indexOf(DDD) != -1 && DDD.length == 2) {
        response = { valid: false, data: 'Número do DDD inválido' }
      }

      if (["1", "2", "3", "4","5"].indexOf(value.substring(2, 3)) == -1 && value.lenght > 2) {
        response = { valid: false, data: 'Número de telefone começa com digíto inválido' }
      }

      if (value.length > 0 && ![10, 11].includes(value.length)) {
        response = { valid: false, data: 'Tamanho do número inválido' }
      }

      if (value == '0000000000') {
        response = { valid: false, data: 'Número inválido' }
      }

      resolve(response)
    })
  }
})

Validator.extend('pendencyType', {
  getMessage(fields, args) { return 'Tipo de pendência é obrigatório.' },
  validate(value, args) { return value.type != null }
})

Validator.extend('date', {
  getMessage(fields, args) { return 'Não é uma data válida' },
  validate(value, args) { return _.isDate(value) }
})

Validator.extend('pendencyDate', {
  getMessage(fields, args) { return 'Data de expiração da pendência é obrigatória.' },
  validate(value, args) { return value.expiresAt != null }
})

Validator.extend('pendencyDateFuture', {
  getMessage(fields, args) { return 'Data de expiração da pendência não pode ser anterior à data de criação da postagem.' },
  validate(value, args) {
    let createdAt = args[0]
    let expiresAt

    if(createdAt != 'undefined') {
      createdAt = new Date(createdAt)
    } else {
      createdAt = new Date()
      createdAt.setHours(0,0,0,0)
    }

    if(_.isString(value.expiresAt)) {
      expiresAt = new Date(value.expiresAt)
    } else {
      expiresAt = value.expiresAt
    }

    return expiresAt >= createdAt
  }
}) 

Validator.extend('galleryUpload', {
  getMessage(fields, args) { return 'É necessário mais de uma imagem para criar a galeria' },
  validate(value, args) { 
    return value.length > 1
  }
})

Validator.extend('bannerUpload', {
  getMessage(fields, args) { return 'A imagem do banner é obrigatória' },
  validate(value, args) { 
    return value.length == 1
  }
})

function SanitizeDate(date) {
  if (!date) {
    return null
  }

  if (date == 'now') {
    return new Date().getTime()
  }

  if (_.isString(date) && date.includes('-')) {
    return new Date(date).getTime() || null
  }

  return new Date(parseInt(date)).getTime() || null
}

Validator.extend('endDateFuture', {
  getMessage(fields, args) { return 'Data final do evento deve ser maior que data inicial.' },
  validate(value, args) {
    // Event End Date
    let startDate = SanitizeDate(args[0])
    let endDate = SanitizeDate(value)

    if(!endDate || !startDate) {
      return false
    }

    return startDate <= endDate
  }
}) 