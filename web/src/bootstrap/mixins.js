
import Vue from 'vue'

import VueDialog from '@/helpers/VueDialog'
Vue.mixin(VueDialog)

import VueTimers from '@/helpers/VueTimers'
Vue.mixin(VueTimers)