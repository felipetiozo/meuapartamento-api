import Axios from 'axios'
import _ from 'lodash'

function Interest() { }

Interest.prototype.create = async function(payload) {
  return await Axios.post('/interest', payload)
}

export default (new Interest())