import Axios from 'axios'
import _ from 'lodash'

function Contract() { }

Contract.prototype.create = async function(payload) {
  return await Axios.post('/contract', payload)
}

Contract.prototype.publish = async function(contractId) {
  return await Axios.post(`/contract/${contractId}/publish`, )
}

Contract.prototype.update = async function(contractId, payload) {
  return await Axios.put(`/contract/${contractId}`, payload)
}

Contract.prototype.resendEmail = async function(contractId) {
  return await Axios.get(`/contract/${contractId}/resend`)
}

Contract.prototype.list = async function(filter) {
  return await Axios.get('/contract', {
    params: filter
  })
}

Contract.prototype.view = async function(contractId) {
  return await Axios.get(`/contract/${contractId}`)
}

export default (new Contract())