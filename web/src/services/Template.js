import Axios from 'axios'
import _ from 'lodash'

function Template() { }

Template.prototype.list = async function() {
  return await Axios.get('/template')
}

Template.prototype.remove = async function(id) {
  return await Axios.delete('/template/' + id)
}

Template.prototype.create = async function(payload) {
  return await Axios.post('/template', payload)
}

Template.prototype.view = async function(id) {
  return await Axios.get('/template/' + id)
}

Template.prototype.refresh = async function(id) {
  return await Axios.get('/template/' + id + '/refresh')
}

export default (new Template())