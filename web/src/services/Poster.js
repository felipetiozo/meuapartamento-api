import Axios from 'axios'
import _ from 'lodash'

function Poster() { }

Poster.prototype.list = async function() {
  return await Axios.get('/poster')
}

export default (new Poster())