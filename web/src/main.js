// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been se fixt in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import '@/bootstrap/raven'
import '@/bootstrap/axios-interceptor'
import '@/bootstrap/plugins'
import '@/bootstrap/mixins'
import '@/bootstrap/filters'
import '@/bootstrap/validators'
import '@/bootstrap/css'

Vue.config.productionTip = false

import router from '@/router'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})