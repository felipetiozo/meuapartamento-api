<p align="center">
  <img src="https://cdn.edu.tenda.digital/admin/uploads/ac58d175-0a99-4c96-bffd-0b9c4de3920d/Frame%201%20(1).png" height="64" /><br /><br />
  <img src="https://cdn.edu.tenda.digital/admin/uploads/077d636f-a175-4b33-b9f5-65dc5d7be880/ezgif-6-d2f53ffa1579.gif"/>
</p>
<br />

# Meu Apê - API + Interface Web para facilitar a vida de quem procura um apartamento

Demo: https://meuapartamento.herokuapp.com/

* O Meu Apê é um serviço para que consegue juntar anúncios de novos aparmentos de diversas plataformas e filtrar de acordo com o gosto do comprador

* Meu Apê foi construído durante o Loftech Hackathon, que aconteceu em 30/11/2019.

<img src="https://cdn.edu.tenda.digital/admin/uploads/a97bf0f7-fb21-448e-b2f7-955ec3be8a57/Frame%204.png"/>

* Arquitetura técnica da realização do projeto

Rotas para Integração da API

```
# Endpoint para listagem dos anúncios
GET http://meuapartamento.herokuapp.com/poster?answers=<ANSWERS (caso exista)>

{
  "hits": [
        {
      price: 149500000,
      name: 'Alameda Franca 1581',
      description: 'Ap 21 - Jardim América - 2º andar',
      metadata: {
        useful_area: 112,
        rooms: 2,
        garages: 1
      },
      url: 'https://www.loft.com.br/home/zjzjj1',
      media: 'https://content.loft.com.br/homes/zjzjj1/banner_thumbnail.jpg',
      source: 'loft'
    },
    {
      price: 79000000,
      name: 'Rua Tuim 536',
      description: 'Ap 53 - Moema Passáros - 5º andar',
      metadata: {
        useful_area: 55,
        rooms: 2,
        garages: 1
      },
      url: 'https://www.loft.com.br/home/7nf1l0',
      media: 'https://content.loft.com.br/homes/7nf1l0/banner_thumbnail.jpg',
      source: 'loft'
    },
    {
      price: 62000000,
      name: 'Rua Doutor Veiga Filho 815',
      description: 'Ap 74 - Higienópolis - 7º andar',
      metadata: {
        useful_area: 62,
        rooms: 2,
        garages: 1
      },
      url: 'https://content.loft.com.br/homes/wvx805/banner_thumbnail.jpg',
      media: 'https://www.loft.com.br/home/wvx805',
      source: 'loft'
    }
  ],
  "count": 3
}
```

# Tecnologias Utilizadas:
- [Vue.js + PWA](https://vuejs.org/),
- [NodeJS](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)
- [Docker](https://www.docker.com/)
- [Automatic deploy with GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

Integrações:
- [Loft](https://www.loft.com.br/)
- [Zap Imóveis](https://www.zapimoveis.com.br/)
- [Vivareal](https://www.vivareal.com.br/)

Infraestrutura:
- [Heroku](https://www.heroku.com/) - API + WebApp
- [Mlab](https://mlab.com/) - Banco de dados
